package main

import (
	"log"

	"gitlab.com/mudders/betrayal_at_critroll/config"
)

var conf config.Config

func init() {
	conf = config.NewFromCLIFlags()
	conf.RectifyWithEnv()
	log.Printf("Configured with host: %s, port: %s, env: %s\n", conf.Host, conf.Port, conf.Env)
}

func main() {
}
