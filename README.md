Betrayal at CritRoll
====================

A mud engine to bring muds into the future. This mud includes a design studio, multi-game tenancy, a web, websocket, tcp, and coap interface support, as well as a restful interface for game or player stats (perhaps more as we go along).

TBD
---

We are currently fleshing out the features through issues and a bit of playfulness. We'll have requirements and such for you soon enough. In the meantime, enjoy [a campy soundtrack](https://www.youtube.com/watch?v=0U0Lh8-apvc).