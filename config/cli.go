package config

import (
	"flag"
)

// NewFromCLIFlags parses the flags passed in from the CLI and returns a config from them
func NewFromCLIFlags() Config {
	c := Config{}
	flag.StringVar(&c.Host, "h", defaultHost, "host the server is running on")
	flag.StringVar(&c.Host, "host", defaultHost, "host the server is running on")
	flag.StringVar(&c.Port, "p", defaultPort, "port the server is running on")
	flag.StringVar(&c.Port, "port", defaultPort, "port the server is running on")
	flag.StringVar(&c.Env, "e", defaultEnv, "environment the server is running")
	flag.StringVar(&c.Env, "env", defaultEnv, "environment the server is running")
	flag.Parse()
	return c
}
