package config

import (
	"os"
)

type Config struct {
	Host, Port, Env string
}

func New() Config {
	return Config{Host: defaultHost, Port: defaultPort}
}

func (c *Config) RectifyWithEnv() {
	if v, ok := os.LookupEnv("BAC_HOST"); ok {
		c.Host = v
	}
	if v, ok := os.LookupEnv("BAC_PORT"); ok {
		c.Port = v
	}
	if v, ok := os.LookupEnv("BAC_ENV"); ok {
		c.Env = v
	}
}
