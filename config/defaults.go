package config

var (
	defaultHost = "localhost"
	defaultPort = "9000"
	defaultEnv  = "development"
)
